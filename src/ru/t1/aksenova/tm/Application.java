package ru.t1.aksenova.tm;

import static ru.t1.aksenova.tm.constant.TerminalConst.*;

public class Application {

    public static void main(final String[] args) {
        System.out.println("** WELCOME TASK MANAGER **");
        processArguments(args);
    }

    public static void processArguments(final String[] args) {
        if (args == null || args.length == 0) {
            showError();
            return;
        }
        processArgument(args[0]);
    }

    public static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ABOUT:
                showAbout();
            break;
            case VERSION:
                showVersion();
            break;
            case HELP:
                showHelp();
            break;
        }
    }

    public static void showError() {
        System.err.println("[ERROR]");
        System.err.println("This argument not supported...");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Anastasiya Aksenova");
        System.out.println("e-mail: aaksenova@t1-consulting.ru");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.2.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show about program. \n", ABOUT);
        System.out.printf("%s - Show program version. \n", VERSION);
        System.out.printf("%s - Show list arguments. \n", HELP);
    }

}
